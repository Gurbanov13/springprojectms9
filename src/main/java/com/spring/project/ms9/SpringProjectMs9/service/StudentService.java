package com.spring.project.ms9.SpringProjectMs9.service;

import com.spring.project.ms9.SpringProjectMs9.dto.StudentDto;

import java.util.ArrayList;

public interface StudentService {

    StudentDto getStudent(Long id);

    ArrayList<StudentDto> getAllStudent();

    StudentDto createstudent(StudentDto dto);

    StudentDto updateStudent(StudentDto dto);

    Boolean deleteStudent(Long id);
}
