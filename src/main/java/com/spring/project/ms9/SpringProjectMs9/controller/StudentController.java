package com.spring.project.ms9.SpringProjectMs9.controller;

import com.spring.project.ms9.SpringProjectMs9.dto.StudentDto;
import com.spring.project.ms9.SpringProjectMs9.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService service;

    @GetMapping("/getallstudent")
    public ArrayList<StudentDto> getAllStudent() {
        return service.getAllStudent();
    }

    @GetMapping("/getstudent/{id}")
    public StudentDto getStudent(@PathVariable Long id) {
        return service.getStudent(id);
    }


    @PostMapping("/create")
    public StudentDto createStudent(@RequestBody StudentDto dto) {

        StudentDto createdDto = service.createstudent(dto);

        return createdDto;
    }

    @PutMapping("/update")
    public StudentDto updateStudent(@RequestBody StudentDto dto) {
        StudentDto updatedDto = service.updateStudent(dto);
        return updatedDto;
    }

    @DeleteMapping("/delete/{id}")
    public String deleteStudent(@PathVariable Long id) {
        Boolean isDeleted = service.deleteStudent(id);
        if (isDeleted) {
            return "The Student is deleted";
        } else {
            return "The Student is not deleted";
        }

    }
}
