package com.spring.project.ms9.SpringProjectMs9.service;

import com.spring.project.ms9.SpringProjectMs9.dto.StudentDto;
import com.spring.project.ms9.SpringProjectMs9.model.Student;
import com.spring.project.ms9.SpringProjectMs9.repositery.StudentRepositery;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ServiceStudent implements StudentService {

    private final StudentRepositery studentRepositery;
    private final ModelMapper mapper;

    @Override
    public StudentDto getStudent(Long id) {
        Student studentModelById = studentRepositery.findById(id).orElseThrow(() -> new RuntimeException("Student not found"));
        StudentDto studentDtoById = mapper.map(studentModelById, StudentDto.class);
        return studentDtoById;
    }

    @Override
    public ArrayList<StudentDto> getAllStudent() {

        List<Student> studentList = studentRepositery.findAll();
        ArrayList<StudentDto> studentDtoList = new ArrayList<StudentDto>();
        studentList.forEach((student) -> {
            studentDtoList.add(mapper.map(student, StudentDto.class));
        });
        return studentDtoList;
    }

    @Override
    public StudentDto createstudent(StudentDto dto) {
        Student createstudent = mapper.map(dto, Student.class);

        createstudent = studentRepositery.save(createstudent);

        return  mapper.map(createstudent, StudentDto.class);
    }

    @Override
    public StudentDto updateStudent(StudentDto dto) {
        Student updateStudent = studentRepositery.findById(dto.getId()).orElseThrow(() -> new RuntimeException("Student Not Found"));
        updateStudent = mapper.map(dto, Student.class);

        updateStudent = studentRepositery.save(updateStudent);

        return mapper.map(updateStudent, StudentDto.class);

    }

    @Override
    public Boolean deleteStudent(Long id) {

        try {
            studentRepositery.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }

    }
}
