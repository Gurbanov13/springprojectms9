package com.spring.project.ms9.SpringProjectMs9.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloRestController {

    @GetMapping("/hello/{name}")
    public  String getHelloWorld(@PathVariable String name){
        String hello= "Hello " + name;
        return  hello;
    }
}
