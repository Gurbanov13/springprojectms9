package com.spring.project.ms9.SpringProjectMs9.repositery;

import com.spring.project.ms9.SpringProjectMs9.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepositery extends JpaRepository<Student,Long> {
}
