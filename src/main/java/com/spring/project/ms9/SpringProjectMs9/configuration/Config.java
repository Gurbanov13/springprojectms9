package com.spring.project.ms9.SpringProjectMs9.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class Config {

    @Bean
    public ModelMapper getModelMapper(){
        return new ModelMapper();
    }
/*
    @Bean
    public DataSource datasource() {
        return DataSourceBuilder.create()
                .driverClassName("com.mysql.cj.jdbc.Driver")
                .url("jdbc:mysql://localhost:3306/ms9")
                .username("root")
                .password("password")
                .build();
    }*/
}
