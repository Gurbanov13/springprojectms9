package com.spring.project.ms9.SpringProjectMs9;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class SpringProjectMs9Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringProjectMs9Application.class, args);
	}

}
